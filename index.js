let seconds = 0;
const button = document.getElementById("startInterval");
const h1 = document.createElement("h1");
h1.style.fontSize = "50px";
// const h1 = document.getElementById("counter");

button.onclick = function () {
  button.remove();
  document.body.appendChild(h1);
  h1.innerText = seconds;
  seconds = 1; // bez tego zero utrzymuje się przez 2 sekundy a nie jedną
  setInterval(function () {
    h1.innerText = seconds;
    seconds++;
  }, 1000);
};

// button.onclick = function () {
//   h1.innerText = seconds;
//   setInterval(function () {
//     h1.innerText = seconds;
//     seconds++;
//   }, 1000);
// };
